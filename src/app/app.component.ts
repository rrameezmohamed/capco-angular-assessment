import { Component, OnInit } from '@angular/core';
import { SampledataService } from './sampledata.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  sampleData: any;
  // pager object
  pagination: any = {};
  // paged items
  paginationData: any[];
  pageSize = 10;
  dataPerPage = [10, 25, 50, 75, 100];

  constructor(private sampledataService: SampledataService) { }
  title = 'Angular Assessment';

  setPage(page: number) {
    // get pager object from service
    this.pagination = this.sampledataService.getPager(this.sampleData.length, page, this.pageSize);
    // get current page of items
    this.paginationData = this.sampleData.slice(this.pagination.startIndex, this.pagination.endIndex + 1);
  }

  selectDataPerPage(pageSize) {
    this.pageSize = +pageSize;
    this.setPage(1);
  }

  ngOnInit() {
    this.Init();
  }

  Init() {
    this.sampledataService.GetSampleData()
      .subscribe(data => {
        this.sampleData = data;
        // log response to console.
        console.log(data);
        this.setPage(1);
      });
  }
}
